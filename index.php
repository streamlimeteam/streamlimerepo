<?php 
	$page_id=1;
	include('includes/header.php'); 
?>
    <div class="banner">
        <div class="container">
                <div class="text caption"> 
                    <h3>Engineering Future</h3>
                    <h5>Managed by IIT and NIT Engineers</h5>
                </div>
        </div>
        <div id="banner_wrap"></div>
        <!--<div class="container">
            <div class="caption">
                <h3>Engineering</h3>
                <h2>Future</h2>
                <h5>Managed by IIT and NIT Engineers</h5> </div>
        </div>-->
        <div class="service_button"> <a href="javascript: void(0)">Services</a> </div>
        <!--<div class="service_menu scroll" data-aos="fade-left" data-aos-offset="500"
     data-aos-easing="ease-in-sine">-->
            <div class="service_menu scroll">
             <div class="bar_parent"><span><i>Services</i></span></div>
            <ul>
                <li><a href="service.php">Structural Design of RCC Building</a></li>
                <li><a href="service.php">Structural Design of Steel Building</a></li>
                <li><a href="service.php">Structural Design of Over Head Water Tanks</a></li>
                <li><a href="service.php">Structural Design of Bridges</a></li>
                <li><a href="service.php">Post tension works</a></li>
                <li><a href="service.php">Pretension works</a></li>
                <li><a href="service.php">Flatslab Design</a></li>
                <li><a href="service.php">Masonry Design</a></li>
                <li><a href="service.php">GFRG design</a></li>
                <li><a href="service.php">Planning and Estimation</a></li>
                <li><a href="service.php">Design based on international codes</a></li>
                <li><a href="service.php">Training / Internships in Structural Analysis and Design</a></li>
                <li><a href="service.php">Structural Proof Checking/ Scrutiny</a></li>
                <li><a href="service.php">Outsourcing</a></li>
            </ul>
        </div>
    </div>
    <div class="streamline_cont">
        <div class="container">
            <div class="content_box" data-aos="fade-up">
                <div class="we_are">
                    <h1>We are leaders in construction</h1>
                    <p>Streamline consortium is a Kerala based company providing the structural design consultancy in India and Globally since 1998. 
					The firm is founded by Er. K.S Binod, a civil engineering graduate from Regional Engineering College, Kozhikode (REC/NIT Calicut) 1993 pass out. 
					He is having more than 20 years of experience in the field of structural engineering.
					<br><br>We have earned a reputation for designing large structural engineering projects. The Company has given consultancy service for commercial &amp; 
					multistoried Residential Building, Hospitals, Malls, Religious buildings, Educational institutions, auditoriums, Hotels, Stadiums, Water tanks, 
					Industrial Structures and Public Infrastructures.  As a Design Consultancy firm, we have rendered our services to some of the foremost urban spaces 
					and systems in Kerala and all over India. The breadth and amount of our work earmark our goal to give the best. We ensure diversity in our projects that our clients entrust upon us.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="let_grow">
        <div class="container">
            <div class="training_ads">
                <p>TRAINING &amp; DEVELOPMENT</p>
                <h1>Let’s Grow With Us</h1> <a href="training-cell.php">touch with us</a> </div>
        </div>
    </div>
    <div class="our_portfolio">
        <div class="container">
            <div class="portfolio_gallery">
                <div class="portfolio_head">
                    <h1>OUR portfolio</h1> </div>
                <div class="demo-gallery">
                    <ul id="slide-gallery" class="list-unstyled">
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Apartment-at-lakkidi.jpg" data-sub-html="<h4>Apartment</h4><p>Lakkidi</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Apartment-at-lakkidi.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Apartment</h3>
                                        <span>Lakkidi</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/AUDITORIUM-at-thalikulam.jpg" data-sub-html="<h4>AUDITORIUM</h4><p>Thalikulam</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/AUDITORIUM-at-thalikulam.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>AUDITORIUM</h3>
                                        <span>Thalikulam</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/KPM-Commercial-at-Calicut.jpg" data-sub-html="<h4>KPM Commercial</h4><p>Calicut</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/KPM-Commercial-at-Calicut.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>KPM Commercial</h3>
                                        <span>Calicut</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Osco-Apartment-at-Calicut.jpg" data-sub-html="<h4>Osco Appartment</h4><p>Calicut</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Osco-Apartment-at-Calicut.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Osco Appartment</h3>
                                        <span>Calicut</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Palakad-District-Cooperative-Hospital-5.jpg" data-sub-html="<h4>District Co-operative Hospital</h4><p>Palakad</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Palakad-District-Cooperative-Hospital-5.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>District Co-operative Hospital</h3>
                                        <span>Palakad</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-MALAPPURAM-(2).jpg" data-sub-html="<h4>RESIDENCE</h4><p>Malappuram</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-MALAPPURAM-(2).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>RESIDENCE</h3>
                                        <span>Malappuram</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-trivandrum-(3).jpg" data-sub-html="<h4>residence</h4><p>Trivandrum</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-trivandrum-(3).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>residence</h3>
                                        <span>Trivandrum</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-Varkala-(3).jpg" data-sub-html="<h4>Residence</h4><p>Varkala</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-Varkala-(3).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Varkala</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Vzone-Commercial-at-Calicut.jpg" data-sub-html="<h4>Vzone Commercial</h4><p>Calicut</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Vzone-Commercial-at-Calicut.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Vzone Commercial</h3>
                                        <span>Calicut</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
                <div class="portfolio_button"> <a href="portfolio.php">View all</a> </div>
            </div>
        </div>
    </div>
    <div class="career_openiings">
        <div class="container">
            <div class="career_ads">
                <h3>Your BluePrint  For</h3>
                <h1>The Future</h1> <a href="career.php">CAREER OPENIINGS</a> </div>
        </div>
    </div>
    <?php include('includes/footer.php');?>
    <script>
        $(document).ready(function () {
            $('#slide-gallery').lightGallery();
            /*$("#ghost").ghosttyper({
                messages: ['Future']
                , timeWrite: 200
                , timeDelete: 100
                , timePause: 8000
            });*/
            /*$(document).foundation();  
      $(document).animateScroll();
    

      var doc = document.documentElement;
      doc.setAttribute('data-useragent', navigator.userAgent);*/
        });
    </script>
</body>

</html>