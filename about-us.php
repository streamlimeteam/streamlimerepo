<?php 
	$page_id=2;
	include('includes/header.php'); 
?>
    <div class="about_part">
        <div class="head_page">
            <div class="container">
                <h2>About Us</h2> </div>
        </div>
        <div class="about_streamline">
            <div class="container">
                <div class="content_box">
                    <!-- <div class="about_img">
                    <img src="images/4895530fa0a856d853cff6319f9b235d.jpg" alt="streamline">
                </div>-->
                    <div class="boder" data-aos="fade-left">
                        <div class="we_are">
                            <h1>We are leaders in construction</h1>
                            <p>Streamline consortium is a Kerala based company providing the structural design consultancy in India and Globally since 1998. The firm is founded by <b>Er. K.S Binod</b>, a civil engineering graduate from Regional Engineering College, Kozhikode (REC/NIT Calicut) 1993 pass out. He is having more than 20 years of experience in the field of structural engineering. He is leading the company to great heights with more than 20 years of experience in the field of structural design. He is experienced in the design of a wide variety of building types including retail centers, office buildings, schools, hospitals, university campuses, residential housing, high rise buildings and renovation and retrofitting of structures. He has thorough understanding of construction economy and its application. His expertise lies in developing innovative, sound structural systems which appeal to the architectural aesthetics
                                <br>
                                <br> We have earned a reputation for designing large structural engineering projects. The Company has given consultancy service for commercial &amp; multistoried Residential Building, Hospitals, Malls, Religious buildings, Educational institutions, auditoriums, Hotels, Stadiums, Water tanks, Industrial Structures and Public Infrastructures. As a Design Consultancy firm, we have rendered our services to some of the foremost urban spaces and systems in Kerala and all over India. The breadth and amount of our work earmark our goal to give the best. We ensure diversity in our projects that our clients entrust upon us.</p>
                            <ul>
                                <li>We have a team of highly skilled and experienced engineers in India.</li>
                                <li>We have a team of graduates/post graduates from premier institutes like IIT’s and NIT’s.</li>
                                <li>Our team is well versed in Indian Standards and practice and also has the knowledge in international standards.</li>
                                <li>Our team is well versed in various structural software.</li>
                                <li>We are having a strict system of Quality Control for all stages of designs performed at our office, developed over years of experience of working in India. All design activities are closely monitored to provide error-free and optimal designs.</li>
                                <li>We are also conscious of the importance of time bound work, and ensure that our designs are delivered in time even under the tightest schedules.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="rd_catgories">
                <div class="container">
                    <h1>OTHER KEY PERSONALS</h1>
                    <div class="row">
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="rd_box">
                                <div class="rd_bx_cont">
                                    <h3>Er. Ratheesh M. R - Principal structural Consultant</h3> <span>M.Tech- NIT/REC Calicut</span> <span>B.Tech – CET Trivandrum</span>
                                    <p>Mr. Ratheesh is a graduate from college of engineering, Trivandrum, Kerala (CET) and

postgraduate from National Institute of Technology, Calicut, Kerala (REC/NIT Calicut). He has

more than 8 years of work experience in India and Middle East. He has vast experience in the

field of steel design and foundation design. He is experienced in the design of a wide variety of

structures including housing developments, multi storied residential and hotel buildings,

shopping malls, educational institutions, hospitals, industrial buildings, religious institutions,

water treatment plants and renovation, rehabilitation and retrofitting of structures. His expertise

lies in the design of steel and composite structures and intricate foundation designs. He has been

associated with various projects like the 50 storey JW Marriott Tower (Bahrain), King Faisal

Foot Bridge (Bahrain), St. Peter’s Church (Kannur), 30 storey Capricorn Tower (Bahrain), Diyar

Al Muharraq (Bahrain), the Seef Mall (Bahrain), Zayani Motors Showroom (Bahrain), Buhair

Mosque (Bahrain), 18 storey Seef Hotel (Bahrain) and Waste Water Treatment Plant for the

Mascot Group (Kannur). He is well versed with various structural design and analysis softwares

like STAAD PRO, PROKON, TEDDS, Finite Element Software (ANSYS), MATLAB-7, SAFE,

AutoCAD. He is also well versed in various international design codes.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-6 col-md-6 col-sm-6">
                            <div class="rd_box">
                                <div class="rd_bx_cont">
                                    <h3>Er. Rojan Mathew - Chief structural Consultant</h3> <span>M.Tech- IIT Gandhinagar</span> <span>MBA – SMU Manipal</span> <span>B.Tech- NIT/REC Calicut</span>
                                    <p>Mr. Rojan Mathew is a post graduate from Indian Institute of Technology, Gandhinagar, Gujrat

(IIT Gandhinagar) and Graduate from National Institute of Technology, Calicut, Kerala

(REC/NIT Calicut). He is an expertise in the field of seismic design and dynamic analysis. After

completion of studies, he was working in Ahmedabad. He is is well versed with various

structural design and analysis softwares like STAAD PRO, SAP2000, ETABS, SAFE,

MATLAB, AUTOCAD etc. He has experience in the seismic design of water tanks, multistory

residential and commercial buildings, shopping malls, educational institutions, hospitals

retaining structures like diaphragm wall.</p>
                                </div>
                            </div>
                        </div>
                        <!--<div class="col-lg-4 col-md-4 col-sm-4">
                            <div class="rd_box">
                                <div class="rd_bx_cont">
                                    <h3>Er. K.S. Binod</h3> <span>B.Tech – NIT/REC Calicut</span>
                                    <p>Mr. K. S. Binod is the founder and managing director of Streamline Consortium. He is a

graduate from regional engineering college, Calicut, Kerala (Now NIT Calicut). He is leading the

company to great heights with more than 20 years of experience in the field of structural design.

He is experienced in the design of a wide variety of building types including retail centers, office

buildings, schools, hospitals, university campuses, residential housing, high rise buildings and

renovation and retrofitting of structures. He has thorough understanding of construction economy

and its application. His expertise lies in developing innovative, sound structural systems which

appeal to the architectural aesthetics</p>
                                </div>
                            </div>
                        </div>-->
                    </div>
                </div>
            </div>
            <div class="mission_part">
                <div class="container">
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2">
                            <div class="mission">
                                <h1>Mission & Vision</h1>
                                <p>Our mission is to achieve the height of excellence in the sector of civil engineering by offering world class design, innovations, service and building up a strong and reputed client base.</p>
                                <p>We will be always aiming towards raising our benchmark of quality, service and customer satisfaction.</p>
                            </div>
                        </div>
                        <!-- <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="vision">
                            <h1>Vision</h1>
                            <p>Greater Isn't, kind above multiply give abundantly had also wherein appear. Land fowl be itself forth seas behold fifth and divide whales may man fly won't saw lights. Seasons. Signs. Creeping earth lights.</p>
                        </div> 
                    </div> --></div>
                </div>
            </div>
            
        </div>
    </div>
    <?php include('includes/footer.php');?>
        <script>
            AOS.init({
                easing: 'ease-in-out-sine'
            });
        </script>
        </body>

        </html>