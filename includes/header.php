<!DOCTYPE html>
<html lang="">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="author" content="Bodhi Info Solution Pvt Ltd">
    <meta name="Description" content="Streamline consortium is a Kerala based company providing the structural design consultancy in India and Globally since 1994." />
    <meta name="Keywords" content="Streamline consortium, Structural Design, Architectures in Calicut, Architectures, Structural Designers in Calicut" />
    <title>Streamline Consortium</title>
    <link rel="shortcut icon" href="">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link href="https://fonts.googleapis.com/css?family=Raleway:400,600" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <link rel="stylesheet" href="css/lightgallery.css">
    <link rel="stylesheet" href="css/vegas.css">
    <link rel="stylesheet" href="css/slider.css">
    <link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
    <link rel="stylesheet" href="css/style.css"> </head>

<body>
    <header>
        <div class="container-fluid">
            <div class="logo">
                <a href="index.php"><img src="images/logo.png" alt="Streamline"></a>
            </div>
            <div class="right_contact_menu">
                <div class="top_contact"> 
							<a href="tel:04952303819"><span><i class="fa fa-mobile" aria-hidden="true"></i></span>0495 230 3819</a> <a href="mailto:info@streamlineconsortium.com"><span><i class="fa fa-envelope" aria-hidden="true"></i></span>info@streamlineconsortium.com</a> </div>
                <div class="nav_triger">
                    <div id="nav-icon2"> <span></span> <span></span> <span></span> <span></span> <span></span> <span></span> </div>
                </div>
                <nav class="main_menu mobile">
                    
                <?php if($page_id==1){ ?>
                    <ul>
                        <li><a href="index.php" <?php if($page_id==1){ echo 'class="active"'; } ?>>HOME </a></li>
                        <li><a href="about-us.php" <?php if($page_id==2){ echo 'class="active"'; } ?>>ABOUT US </a></li>
                        <li><a href="service.php" class="service" <?php if($page_id==3){ echo 'class="active"'; } ?>>SERVICES</a></li>
                        <li><a href="portfolio.php" <?php if($page_id==4){ echo 'class="active"'; } ?>>portfolio</a></li>
                        <li><a href="training-cell.php" <?php if($page_id==5){ echo 'class="active"'; } ?>>TRAINING CELL</a></li>
                        <li><a href="r&d.php" <?php if($page_id==6){ echo 'class="active"'; } ?>>R&amp;D</a></li>
                        <li><a href="career.php" <?php if($page_id==7){ echo 'class="active"'; } ?>>CAREER</a></li>
                        <li><a href="contact.php" <?php if($page_id==8){ echo 'class="active"'; } ?>>CONTACT US</a></li>
                    </ul>
                    <?php } ?>
                    <?php if($page_id==2 || $page_id==3 || $page_id==4 || $page_id==5 || $page_id==6 || $page_id==7 || $page_id==8){ ?>
                    <ul>
                        <li><a href="index.php" <?php if($page_id==1){ echo 'class="active"'; } ?>>HOME </a></li>
                        <li><a href="about-us.php"  <?php if($page_id==2){ echo 'class="active"'; } ?>>ABOUT US </a></li>
                        <li><a href="service.php" <?php if($page_id==3){ echo 'class="active"'; } ?> class="service">SERVICES</a>
                            <ul class="sub_menu">
                                <li><a href="service.php">Structural Design of RCC Building</a></li>
                                <li><a href="service.php">Structural Design of Steel Building</a></li>
                                <li><a href="service.php">Structural Design of Over Head Water Tanks</a></li>
                                <li><a href="service.php">Structural Design of Bridges.</a></li>
                                <li><a href="service.php">Post tension works</a></li>
                                <li><a href="service.php">Pretension works</a></li>
                                <li><a href="service.php">Flatslab Design</a></li>
                                <li><a href="service.php">Masonry Design</a></li>
                                <li><a href="service.php">GFRG design</a></li>
                                <li><a href="service.php">Planning and Estimation</a></li>
                                <li><a href="service.php">Design based on international codes.</a></li>
                            </ul>
                        </li>
                        <li><a href="portfolio.php" <?php if($page_id==4){ echo 'class="active"'; } ?>>portfolio</a></li>
                        <li><a href="training-cell.php" <?php if($page_id==5){ echo 'class="active"'; } ?>>TRAINING CELL</a></li>
                        <li><a href="r&d.php" <?php if($page_id==6){ echo 'class="active"'; } ?>>R&amp;D</a></li>
                        <li><a href="career.php" <?php if($page_id==7){ echo 'class="active"'; } ?>>CAREER</a></li>
                        <li><a href="contact.php" <?php if($page_id==8){ echo 'class="active"'; } ?>>CONTACT US</a></li>
                    </ul>
                    <?php } ?>
                </nav>
            </div>
        </div>
    </header>
    
    
    <?php if($page_id==2 || $page_id==3 || $page_id==4 || $page_id==5 || $page_id==6){ ?>
            
    <?php } ?>