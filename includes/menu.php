<div class="main_menu"> <span><i class="fa fa-bars" aria-hidden="true"></i></span>
    <nav>
        <ul>
            <li><a href="index.php">Home</a></li>
            <li><a href="about.php">About us</a></li>
            <li><a href="services.php">Services</a></li>
            <li><a href="doctors.php">Doctors</a></li>
            <li><a href="career.php">Career</a></li>
            <li><a href="contact.php">Contact us</a></li>
        </ul>
    </nav>
</div>