    <footer>
        <div class="container">
            <nav class="footer_menu">
                <ul>
                    <li><a href="index.php">HOME </a></li>
                    <li><a href="about-us.php">ABOUT US </a></li>
                    <li><a href="service.php">SERVICES</a></li>
                    <li><a href="portfolio.php">portfolio</a></li>
                    <li><a href="training-cell.php">TRAINING CELL</a></li>
                    <li><a href="r&d.php">R&amp;D</a></li>
                    <li><a href="career.php">CAREER</a></li>
                    <li><a href="contact.php">CONTACT US</a></li>
                </ul>
            </nav>
            <div class="row foot_content">
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="foot_logo"> <img src="images/logo.png" alt="Streamline"> </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="foot_adrs">
                        <p>Kozhikode (Head Office)
							<br>K M arcade, 19/124-Z Thali - Puthiyapalam Road<br>
							Opposite to NSS College East Thali, PO chalapuram,
                            Palayam, Kozhikode Kerala - 673002<br>
							<a href="tel:04952303819">0495 230 3819</a>
							<br><a href="mailto:info@streamlineconsortium.com">info@streamlineconsortium.com</a></p>
                    </div>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <div class="foot_adrs branch">
					    <h3>BRANCHES</h3>
                        <p>Perinthalmanna (Malappuram)
						   <br>Trivandrum	</p>						
                    </div>
                    <div class="social_media"> <a href="#"><span><i class="fa fa-facebook" aria-hidden="true"></i></span></a> <a href="#"><span><i class="fa fa-twitter" aria-hidden="true"></i></span></a> <a href="#"><span><i class="fa fa-instagram" aria-hidden="true"></i></span></a> </div>
                </div>
            </div>
            <div class="copyright">
                <div class="copy">
                    <p>Copyright © 2017 Streamline Consortium. All Rights Reserved.</p>
                </div>
                <div class="powered"> <span>Powered by</span>
                    <a href="http://bodhiinfo.com/" target="_blank"><img src="images/bodhi.png" alt="bodhi"></a>
                </div>
            </div>
        </div>
    </footer>
    <script src="js/jquery-3.1.1.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/lightgallery.js"></script>
    <script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>
    <script src="js/vegas.min.js"></script>
    <script src="js/ghost-typer.js"></script>
    <script src="js/slider.js"></script>
    <script src="js/scripts.js"></script>