
                   <ul id="slide-gallery4" class="list-unstyled">
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Proposed-School-building-at-ramanatukara-(2).jpg" data-sub-html="<h4>Proposed School building</h4><p>Ramanatukara</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Proposed-School-building-at-ramanatukara-(2).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Proposed School building</h3>
                                        <span>Ramanatukara</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Proposed-School-building-at-ramanatukara.jpg" data-sub-html="<h4>Proposed School building</h4><p>Ramanatukara</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Proposed-School-building-at-ramanatukara.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Proposed School building</h3>
                                        <span>Ramanatukara</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Apartment-at-Calicut.jpg" data-sub-html="<h4>Apartment</h4><p>Calicut</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Apartment-at-Calicut.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Apartment</h3>
                                        <span>Calicut</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-ayancheri.jpg" data-sub-html="<h4>Residence</h4><p>Ayancheri</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-ayancheri.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Ayancheri</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-AZHINZHILAM.jpg" data-sub-html="<h4>Residence</h4><p>Azhinzhilam</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-AZHINZHILAM.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Azhinzhilam</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-CALICUT-(2).jpg" data-sub-html="<h4>Residence</h4><p>Calicut</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-CALICUT-(2).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Calicut</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-CALICUT.jpg" data-sub-html="<h4>Residence</h4><p>Calicut</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-CALICUT.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Calicut</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-chegala.jpg" data-sub-html="<h4>Residence</h4><p>Chegala</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-chegala.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Chegala</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-Chemmad.jpg" data-sub-html="<h4>Residence</h4><p>Chemmad</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-Chemmad.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Chemmad</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-CHERTHALA.jpg" data-sub-html="<h4>Residence</h4><p>Cherthala</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-CHERTHALA.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Cherthala</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-Edappal.jpg" data-sub-html="<h4>Residence</h4><p>Edappal</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-Edappal.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Edappal</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-ELANGODE.jpg" data-sub-html="<h4>Residence</h4><p>Elangode</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-ELANGODE.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Elangode</span>
                                    </div>
                                </a>
                            </div>
                        </li>
</ul>