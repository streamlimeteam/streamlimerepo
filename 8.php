
                   <ul id="slide-gallery7" class="list-unstyled">
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-OCHIRA.jpg" data-sub-html="<h4>Residence</h4><p>Ochira</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-OCHIRA.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Ochira</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-PALAKKAD.jpg" data-sub-html="<h4>Residence</h4><p>Palakkad</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-PALAKKAD.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Palakkad</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-PERAVOOR.jpg" data-sub-html="<h4>Residence</h4><p>Peravoor</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-PERAVOOR.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Peravoor</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-TRIVANDRAM.jpg" data-sub-html="<h4>Residence</h4><p>Trivandram</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-TRIVANDRAM.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Trivandram</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-trivandrum-(3).jpg" data-sub-html="<h4>Residence</h4><p>Trivandram</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-trivandrum-(3).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Trivandram</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-VADAKARA-(2).jpg" data-sub-html="<h4>Residence</h4><p>Vadakara</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-VADAKARA-(2).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Vadakara</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/APPARTMENT-at-PULIKKAL.jpg" data-sub-html="<h4>Apartment</h4><p>Pulikkal</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/APPARTMENT-at-PULIKKAL.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Apartment</h3>
                                        <span>Pulikkal</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Appartment-Perinthalmanna.jpg" data-sub-html="<h4>Apartment</h4><p>Perinthalmanna</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Appartment-Perinthalmanna.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Appartment</h3>
                                        <span>Perinthalmanna</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Atlas-mall-Manjeri-(2).jpg" data-sub-html="<h4>Atlas Mall</h4><p>Manjeri</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Atlas-mall-Manjeri-(2).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Atlas Mall</h3>
                                        <span>Manjeri</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCEAT-MALAPPURAM.jpg" data-sub-html="<h4>Residence</h4><p>Malappuram</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCEAT-MALAPPURAM.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Malappuram</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-Wandoor-(2).jpg" data-sub-html="<h4>Residence</h4><p>Wandoor</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-Wandoor-(2).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Wandoor</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-wandoor.jpg" data-sub-html="<h4>Residence</h4><p>Wandoor</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-wandoor.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Wandoor</span>
                                    </div>
                                </a>
                            </div>
                        </li>
</ul>