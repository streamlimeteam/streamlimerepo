<?php
    header( "refresh:5;url=training-cell.php"); 
?>
<!DOCTYPE HTML>
<html lang="">

<head>
    <style>
        h1{
            font-family: 'Raleway', sans-serif;
            font-size: 22px;
            text-align: center;
            margin-top: 150px;
        }
        h4{
            font-family: 'Raleway', sans-serif;
            font-size: 14px;
            text-align: center;
        }
        .sorry{
            color: firebrick;
        }
        .done{
            color: forestgreen;
        }
        .counter {
            width:100px;
            height:100px;
            /*background:#ccc;*/
            overflow:hidden;
            margin: 0 auto;
        }
        .numbers{
            width:auto;
            white-space:nowrap;
            -webkit-animation: countNumber 10s;
            -webkit-animation-fill-mode:forwards;
            -webkit-animation-timing-function: steps(10);
            font-size: 28px;
            font-family: 'Roboto', sans-serif;
        }
        .numbers div {
            float:left;
            text-align:center;
            width:100px;
            height:100px;
            line-height:100px;
            display:inline-block;
        }

        @-webkit-keyframes countNumber {
            0% {
                margin-left:0px
            }
            100% {
                margin-left:-1000px
            }
}
    </style>
</head>
<body>
<?php
if($_POST) {
	
    $emailTo = 'streamlineclttd@gmail.com';

    $name 		 = trim(ucfirst(strtolower($_POST['name'])));
    $email		 = trim($_POST['email']);
   /* $subject1	 = trim($_POST['subject']);*/
    $phone       = trim($_POST['phone']);
    $message1 	 = trim($_POST['message']);
    
    $subject	 = "Streamline Consortium Training Enquiry";
     // Send email\
    $nn=explode(" ",$name);
	$headers = "From: " . $nn[0]. " <" . $email . ">" . "\r\n" . "Reply-To: " . $email;
	$body 	 = " Name : ".$name."\r\n  Email : ".$email."\r\n Phone : ".$phone."\r\n  About : ".$message1."\r\n";
	$success = mail($emailTo, $subject, $body, $headers);
	if($success)
	{
		 header( "refresh:5;url=training-cell.php" ); 
        echo '<h1 class="done">Your Message has been sent</h1>';
		echo '<h4>You will be directed back soon</h4>';
		echo '<div class="counter">
                <div class="numbers">
                    <div>5</div>
                    <div>4</div>
                    <div>3</div>
                    <div>2</div>
                    <div>1</div>
                    <div>0</div>
                </div>
            </div>';
	}
	else
	{
		 header( "refresh:5;url=training-cell.php" ); 
		echo '<h1 class="sorry">Your Message could not be sent</h1>';
		echo '<h4>You will be directed back soon</h4>';
		echo '<div class="counter">
                <div class="numbers">
                    <div>5</div>
                    <div>4</div>
                    <div>3</div>
                    <div>2</div>
                    <div>1</div>
                    <div>0</div>
                </div>
            </div>';
	}

}

?>
</body>
</html>
<!--
echo '<script language="javascript">';
            echo 'alert("message successfully sent")';
            echo '</script>';-->