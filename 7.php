
                   <ul id="slide-gallery6" class="list-unstyled">
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-MALAPPURAM-(4).jpg" data-sub-html="<h4>Residence</h4><p>Malappuram</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-MALAPPURAM-(4).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Malappuram</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-MALAPPURAM-(6).jpg" data-sub-html="<h4>Residence</h4><p>Malappuram</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-MALAPPURAM-(6).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Malappuram</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-MALAPPURAM.jpg" data-sub-html="<h4>Residence</h4><p>Malappuram</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-MALAPPURAM.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Malappuram</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-Manjeri-(2).jpg" data-sub-html="<h4>Residence</h4><p>Manjeri</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-Manjeri-(2).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Manjeri</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-MANJERI-2.jpg" data-sub-html="<h4>Residence</h4><p>Manjeri</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-MANJERI-2.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Manjeri</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-Manjeri.jpg" data-sub-html="<h4>Residence</h4><p>Manjeri</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-Manjeri.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Manjeri</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-MANKADA-(2).jpg" data-sub-html="<h4>Residence</h4><p>Mankada</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-MANKADA-(2).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Mankada</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-MANKADA.jpg" data-sub-html="<h4>Residence</h4><p>Mankada</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-MANKADA.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Mankada</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-MATTANNUR.jpg" data-sub-html="<h4>Residence</h4><p>Mattannur</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-MATTANNUR.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Mattannur</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/REsidence-at-Melatoor-(2).jpg" data-sub-html="<h4>Residence</h4><p>Melatoor</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/REsidence-at-Melatoor-(2).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Melatoor</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-MELATOOR-1.jpg" data-sub-html="<h4>Residence</h4><p>Melatoor</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-MELATOOR-1.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Melatoor</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/REsidence-at-Melatoor.jpg" data-sub-html="<h4>Residence</h4><p>Melatoor</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/REsidence-at-Melatoor.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Melatoor</span>
                                    </div>
                                </a>
                            </div>
                        </li>
</ul>