<?php
header( "refresh:5;url=career.php" );
?>
<!DOCTYPE HTML>
<html lang="">
<head>
    <meta charset="UTF-8">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<style>
       h1{
            font-family: 'Raleway', sans-serif;
            font-size: 22px;
            text-align: center;
            margin-top: 150px;
        }
        h4{
            font-family: 'Raleway', sans-serif;
            font-size: 14px;
            text-align: center;
        }
        .sorry{
            color: firebrick;
        }
        .done{
            color: forestgreen;
        }
        .counter {
            width:100px;
            height:100px;
            /*background:#ccc;*/
            overflow:hidden;
            margin: 0 auto;
        }
        .numbers{
            width:auto;
            white-space:nowrap;
            -webkit-animation: countNumber 10s;
            -webkit-animation-fill-mode:forwards;
            -webkit-animation-timing-function: steps(10);
            font-size: 28px;
            font-family: 'Roboto', sans-serif;
        }
        .numbers div {
            float:left;
            text-align:center;
            width:100px;
            height:100px;
            line-height:100px;
            display:inline-block;
        }

        @-webkit-keyframes countNumber {
            0% {
                margin-left:0px
            }
            100% {
                margin-left:-1000px
            }
}
	</style>
</head>
<body>
<?php

require_once('PHPMailer/class.phpmailer.php');


if($_POST) 
{
	
	$name		=	trim(ucfirst(strtolower($_POST['name'])));
	$position	=	trim(ucfirst(strtolower($_POST['position'])));
	$message	=	trim(ucfirst(strtolower($_POST['message'])));
	/*$phone		=	trim($_POST['phone']);*/
	$email		=	trim($_POST['email']);
	
	$file		=	$_FILES["resume"]["tmp_name"];
	$filename	=	$_FILES["resume"]["name"];

	$emailTo = 'career@streamlineconsortium.com';
	$subject= "Mail from Streamline Consortium Career";
	$bodytext = " Name : ".$name."\r\n Applying For : ".$position."\r\n Email : ".$email."\r\n A brief about this profile : ".$message."\r\n";
	 
	$email = new PHPMailer();
	$email->From      = "career@streamlineconsortium.com";
	$nn=explode(" ",$name);
	$email->FromName  = $nn[0];
	$email->Subject   = $subject;
	$email->Body      = $bodytext;
	$email->AddAddress($emailTo);

	$file_to_attach = $file;

	$email->AddAttachment( $file_to_attach , $filename );

/*die;*/
	if($email->Send())
	{ 
        echo '<h1 class="done">Your Message has been sent</h1>';
		echo '<h4>You will be directed back soon</h4>';
		echo '<div class="counter">
                <div class="numbers">
                    <div>5</div>
                    <div>4</div>
                    <div>3</div>
                    <div>2</div>
                    <div>1</div>
                    <div>0</div>
                </div>
            </div>';
	}
	else
	{
       
		echo '<h1 class="sorry">Your Message could not be sent</h1>';
		echo '<h4>You will be directed back soon</h4>';
		echo '<div class="counter">
                <div class="numbers">
                    <div>5</div>
                    <div>4</div>
                    <div>3</div>
                    <div>2</div>
                    <div>1</div>
                    <div>0</div>
                </div>
            </div>';
	}
}

?>
</body>
</html>