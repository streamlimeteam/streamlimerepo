
                   <ul id="slide-gallery" class="list-unstyled">
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Apartment-at-Calicut.jpg" data-sub-html="<h4>Apartment</h4><p>Calicut</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Apartment-at-Calicut.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Apartment</h3>
                                        <span>Calicut</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Apartment-at-Kumminiparamba.jpg" data-sub-html="<h4>Apartment</h4><p>Kumminiparamba</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Apartment-at-Kumminiparamba.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Apartment</h3>
                                        <span>Kumminiparamba</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Apartment-at-lakkidi.jpg" data-sub-html="<h4>Apartment</h4><p>Lakkidi</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Apartment-at-lakkidi.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Apartment</h3>
                                        <span>Lakkidi</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                       <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-vagamon.jpg" data-sub-html="<h4>Residence</h4><p>Vagamon</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-vagamon.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Vagamon</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-Varkala-(2).jpg" data-sub-html="<h4>Residence</h4><p>Varkala</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-Varkala-(2).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Varkala</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-Varkala-(3).jpg" data-sub-html="<h4>Residence</h4><p>Varkala</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-Varkala-(3).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Varkala</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Atlas-Mall-Manjeri.jpg" data-sub-html="<h4>Atlas Mall</h4><p>Manjeri</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Atlas-Mall-Manjeri.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Atlas Mall</h3>
                                        <span>Manjeri</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/AUDITORIUM-AT-CHEMMAD.jpg" data-sub-html="<h4>Auditorium</h4><p>Chemmad</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/AUDITORIUM-AT-CHEMMAD.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Auditorium</h3>
                                        <span>Chemmad</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Auditorium-at-Nariparamba.jpg" data-sub-html="<h4>Auditorium</h4><p>Nariparamba</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Auditorium-at-Nariparamba.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Auditorium</h3>
                                        <span>Nariparamba</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/AUDITORIUM-at-thalikulam.jpg" data-sub-html="<h4>Auditorium</h4><p>Thalikulam</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/AUDITORIUM-at-thalikulam.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Auditorium</h3>
                                        <span>Thalikulam</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Auditorium-Edappal.jpg" data-sub-html="<h4>Auditorium</h4><p>Edappal</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Auditorium-Edappal.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Auditorium</h3>
                                        <span>Edappal</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/CHURCH-at-CHENGHANOOR.jpg" data-sub-html="<h4>Church</h4><p>Chenghanoor</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/CHURCH-at-CHENGHANOOR.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Church</h3>
                                        <span>Chenghanoor</span>
                                    </div>
                                </a>
                            </div>
                        </li>
</ul>