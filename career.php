<?php 
	$page_id=7;
	include('includes/header.php'); 
?>
    <div class="career_part">
        <div class="head_page">
            <div class="container">
                <h2>Career</h2> </div>
        </div>
        <div class="container">
            <div class="vacancy">
                <div class="descrip">
                    <p>A well-trained, motivated workforce is the cornerstone of delivering a service that meets with our customers' expectations. The manner in which we recruit, train, develop, retain and care for our employees reflects this belief.
						<br>The organization has long term developmental plans that call for reviving and reshaping the talents internally as well as attracting best of the talents from the outside.
						<br>To increase the pace of our growth, we welcome ambitious, hardworking, experienced men and women with excellent visualization, design capabilities, technical competence and communication skills.</p>
                </div>
            <!--    <div class="curent_vacancy">
                    <h1>Current Vacancy</h1>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="cur_vac_box">
                            <h3>Development Officer</h3>
                            <p>
                                <b>Position:</b> Business Development Officer<br>
                                <b>Vacancies:</b> 5<br>
                                <b>Location:</b> Calicut<br>
                                <b>Experience:</b> Fresher<br>
                                <b>Qualification:</b> Any degree - MBA / B Tech preferable. Result waiting candidates can also apply. Candidate should have excellent communication skill.<br><br>
                            </p>
                               <h4>Job Nature</h4>
                            <p>
                                Develop business and marketing plans in coordination with team to achieve revenue goals.<br>
                                Research the market for identifying new business opportunities.<br>
                                Explain prospective clients about the advantages of the products or services offered and follow up with them in order to close the business deals.<br>
                            </p>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6">
                        <div class="cur_vac_box">
                            <h3>Development Officer</h3>
                            <p>
                                <b>Position:</b> Business Development Officer<br>
                                <b>Vacancies:</b> 5<br>
                                <b>Location:</b> Calicut<br>
                                <b>Experience:</b> Fresher<br>
                                <b>Qualification:</b> Any degree - MBA / B Tech preferable. Result waiting candidates can also apply. Candidate should have excellent communication skill.<br><br>
                            </p>
                                <h4>Job Nature</h4>
                            <p>
                                Develop business and marketing plans in coordination with team to achieve revenue goals.<br>
                                Research the market for identifying new business opportunities.<br>
                                Explain prospective clients about the advantages of the products or services offered and follow up with them in order to close the business deals.<br>
                            </p>
                        </div>
                    </div>
                </div>   -->
            </div>
        </div>
            <div class="form_part">
                <div class="container">
            <div class="upload_resume">
                <h1>Send your CV</h1>
                <form method="post" action="career-mail.php" enctype="multipart/form-data">
                    <input type="text" name="name" placeholder="Name*" required>
                    <input type="email" name="email" placeholder="Email*" required>
                <!--    <select name="position">
                        <option>Select Position</option>
                        <option>Development Officer</option>
                        <option>Officer</option>
                    </select>  -->
                    <textarea name="message" placeholder="A Brife About you"></textarea>
                    <span class="btn btn-default btn-file">Upload Your CV <input type="file" name="resume" value="Upload Your CV"></span>
                    <button type="submit">Submit</button>
                </form>
            </div>
            </div>
        </div>
    </div>
    <?php include('includes/footer.php');?>
    <script>
        $(document).on('change', ':file', function() {
                var input = $(this),
                    numFiles = input.get(0).files ? input.get(0).files.length : 1,
                    label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
                input.trigger('fileselect', [numFiles, label]);
            });
    </script>
</body>

</html>