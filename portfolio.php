<?php 
	$page_id=4;
	include('includes/header.php'); 
?>
    <div class="portfolio_part">
        <div class="head_page">
            <div class="container">
                <h2>Portfolio</h2> </div>
        </div>
        <div class="our_portfolio">
            <div class="container">
                <div class="portfolio_gallery">
                    <div class="intro">
                        <p>We are glad to introduce ourselves as one of the leading structural consultants having more than 20 years of experience in handling various multistory and residential projects. Our head quarter is in Calicut and we are having a branch office at Thiruvanthapuram and Perinthalmanna (Malappuram).</p>
                    </div>
                    <div class="demo-gallery"> <span class="page_active" id="portfolio1"><?php include('1.php');?></span> <span id="portfolio2"><?php include('2.php');?></span> <span id="portfolio3"><?php include('3.php');?></span> <span id="portfolio4"><?php include('4.php');?></span> <span id="portfolio5"><?php include('5.php');?></span> <span id="portfolio6"><?php include('6.php');?></span> <span id="portfolio7"><?php include('7.php');?></span> <span id="portfolio8"><?php include('8.php');?></span> </div>
                </div>
            </div>
            <div class="container">
                <ul class="pagination">
                    <li><a class="active" href="#portfolio1">1</a></li>
                    <li><a href="#portfolio2">2</a></li>
                    <li><a href="#portfolio3">3</a></li>
                    <li><a href="#portfolio4">4</a></li>
                    <li><a href="#portfolio5">5</a></li>
                    <li><a href="#portfolio6">6</a></li>
                    <li><a href="#portfolio7">7</a></li>
                    <li><a href="#portfolio8">8</a></li>
                </ul>
            </div>
        </div>
        <div class="associates">
            <div class="container">
                <p>Presently we are associated with leading consultancies all over Kerala &amp;

Bangalore. To name a few</p>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <ul>
                        <li>Ashok &amp; associates, Thiruvananthapuram</li>
                        <li>Aus architecture, Bangalore</li>
                        <li>Apollo Projects, Calicut</li>
                        <li>Anil &amp; associates, Calicut</li>
                        <li>Amar Associates, Calicut</li>
                        <li>Arif &amp; Associates, Calicut</li>
                        <li>Arun Associates, Kasargod</li>
                        <li>Aakriti Design, Kannur</li>
                        <li>Architechural Studio, Calicut</li>
                        <li>Attiks Architects, Kondotty</li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <ul>
                        <li>BS Architect, Manjeri</li>
                        <li>De Earth, Calicut</li>
                        <li>Ensemblee, Calicut</li>
                        <li>GITPAC, Thiruvananthapuram</li>
                        <li>Great Builders, Thalassery</li>
                        <li>Human Spaces, Mangalore</li>
                        <li>Kaleid architects, Manjeri</li>
                        <li>K.V.Nazir &amp; Associates, Calicut</li>
                        <li>Malabar Developers, Calicut</li>
                        <li>Nazer Associates, Calicut</li>
                    </ul>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-4">
                    <ul>
                        <li>NHD, Calicut</li>
                        <li>90 degree, Calicut</li>
                        <li>Prasanth &amp; associates, Calicut</li>
                        <li>Projem, Calicut</li>
                        <li>PS Designers, Calicut</li>
                        <li>Stapati Architects, Calicut</li>
                        <li>Signature, Calicut</li>
                        <li>Space art, Calicut</li>
                        <li>Shabeer Saleel Associates, Calicut</li>
                        <li>Thought Parallel, Calicut</li>
                        <li>Zero Studio, Manjeri</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <?php include('includes/footer.php');?>
        <script>
            $(document).ready(function () {
                $('#slide-gallery').lightGallery();
                $('#slide-gallery1').lightGallery();
                $('#slide-gallery2').lightGallery();
                $('#slide-gallery3').lightGallery();
                $('#slide-gallery4').lightGallery();
                $('#slide-gallery5').lightGallery();
                $('#slide-gallery6').lightGallery();
                $('#slide-gallery7').lightGallery();
            });
            /*$('.pagination a').click(function(){
                $(this).click(function(){
                    $('.demo-gallery span').removeClass('page_active');
                    $('.demo-gallery span').addClass('page_active');
                    alert("good");
                });
                    
            });*/
            $(document).ready(function () {
                $('.pagination li a').click(function (e) {
                    e.preventDefault();
                    $('.pagination li a').removeClass('active');
                    $(this).addClass('active');
                    $('.demo-gallery span').removeClass('page_active');
                    $($(this).attr('href')).addClass('page_active');
                });
            });
        </script>
        </body>

        </html>