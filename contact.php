<?php 
	$page_id=8;
	include('includes/header.php'); 
?>
        <div class="contact_part">
            <div class="head_page">
                <div class="container">
                    <h2>Contact Us</h2>
                </div>
            </div>
            <div class="contact_information">
                <div class="container">
                    <div class="adrs">
                        <span><i class="fa fa-map-marker" aria-hidden="true"></i></span>
                         <p>Kozhikode (Head Office)
							<br>K M arcade, 19/124-Z, Thali - Puthiyapalam Road
							<br>Opposite to NSS College, East Thali, PO chalapuram
							<br>Palayam, Kozhikode, Kerala - 673002
                            <br><a href="tel:0495 230 3819">0495 230 3819</a>
							<br><a href="mailto:info@streamlineconsortium.com" >info@streamlineconsortium.com</a></p>
                    </div>
                    <div class="map_adrs">
                        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3290.587405839433!2d75.79006996211498!3d11.24743254413012!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3ba65969d62348d5%3A0x728f668f18e90f28!2sStreamline+Consortium!5e0!3m2!1sen!2sin!4v1486790500129" width="100%" height="350" frameborder="0" style="border:0" allowfullscreen></iframe>
                    </div>
                    <div class="feedback_fourm">
                        <h4>Contact Form</h4>
                        <form method="post" action="contactmail.php">
                            <input type="text" name="name" placeholder="Name*" required>
                            <input type="email" name="email" placeholder="Email*" required>
                            <textarea placeholder="Message..." name="message"></textarea>
                            <button type="submit">Send</button>
                        </form>
                    </div>
                </div>
            </div>
    </div>
    <?php include('includes/footer.php');?>
    </body>
</html>