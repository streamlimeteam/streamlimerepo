
                   <ul id="slide-gallery2" class="list-unstyled">
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Commercial-AT-MANKAVU.jpg" data-sub-html="<h4>Commercial</h4><p>Mankavu</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Commercial-AT-MANKAVU.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Commercial</h3>
                                        <span>Mankavu</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Commercial-at-Perambra.jpg" data-sub-html="<h4>Commercial</h4><p>Perambra</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Commercial-at-Perambra.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Commercial</h3>
                                        <span>Perambra</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Commercial-at-Ponnani.jpg" data-sub-html="<h4>Commercial</h4><p>Ponnani</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Commercial-at-Ponnani.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Commercial</h3>
                                        <span>Ponnani</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Commercial-at-Valanchery.jpg" data-sub-html="<h4>Commercial</h4><p>Valanchery</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Commercial-at-Valanchery.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Commercial</h3>
                                        <span>Valanchery</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/COTTAGE-AT-WAYANAD.jpg" data-sub-html="<h4>Cottage</h4><p>Wayanad</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/COTTAGE-AT-WAYANAD.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Cottage</h3>
                                        <span>Wayanad</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Global-building-at-Pantheerankavu.jpg" data-sub-html="<h4>Global Bulding</h4><p>Pantheerankavu</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Global-building-at-Pantheerankavu.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Global Bulding</h3>
                                        <span>Pantheerankavu</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Hospital-at-wandoor.jpg" data-sub-html="<h4>Hospital</h4><p>Wandoor</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Hospital-at-wandoor.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Hospital</h3>
                                        <span>Wandoor</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/HOTEL-AT-WAYANAD.jpg" data-sub-html="<h4>HOTEL</h4><p>Wayanad</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/HOTEL-AT-WAYANAD.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>HOTEL</h3>
                                        <span>Wayanad</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/KPM-Commercial-at-Calicut.jpg" data-sub-html="<h4>KPM Commercial</h4><p>Calicut</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/KPM-Commercial-at-Calicut.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>KPM Commercial</h3>
                                        <span>Calicut</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/malabar-cancerOncology.jpg" data-sub-html="<h4>malabar cancerOncology</h4>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/malabar-cancerOncology.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>malabar cancerOncology</h3>
                                        <!--<span>NYC</span>-->
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/MALL-AT-MANJERI.jpg" data-sub-html="<h4>Mall</h4><p>Manjeri</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/MALL-AT-MANJERI.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Mall</h3>
                                        <span>Manjeri</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Masjid-at-chalissery.jpg" data-sub-html="<h4>Masjid</h4><p>Chalissery</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Masjid-at-chalissery.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Masjid</h3>
                                        <span>Chalissery</span>
                                    </div>
                                </a>
                            </div>
                        </li>
</ul>