
                   <ul id="slide-gallery5" class="list-unstyled">
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-KAKKANCHERI.jpg" data-sub-html="<h4>Residence</h4><p>Kakkancheri</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-KAKKANCHERI.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Kakkancheri</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-kanhagad.jpg" data-sub-html="<h4>Residence</h4><p>kanhagad</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-kanhagad.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>kanhagad</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-KANNUR-(3).jpg" data-sub-html="<h4>Residence</h4><p>Kannur</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-KANNUR-(3).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Kannur</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-KANNUR.jpg" data-sub-html="<h4>Residence</h4><p>Kannur</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-KANNUR.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Kannur</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-KARAKKUNU.jpg" data-sub-html="<h4>Residence</h4><p>Karakkunu</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-KARAKKUNU.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Karakkunu</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-KODUNGALLUR.jpg" data-sub-html="<h4>Residence</h4><p>Kodungallur</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-KODUNGALLUR.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Kodungallur</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-KOTAYAM.jpg" data-sub-html="<h4>Residence</h4><p>Kotayam</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-KOTAYAM.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Kotayam</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-Koyilandy-(2).jpg" data-sub-html="<h4>Residence</h4><p>Koyilandy</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-Koyilandy-(2).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Koyilandy</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/Residence-at-kuttiyadi.jpg" data-sub-html="<h4>Residence</h4><p>Kuttiyadi</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/Residence-at-kuttiyadi.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Kuttiyadi</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-KUTTYADI.jpg" data-sub-html="<h4>Residence</h4><p>Kuttiyadi</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-KUTTYADI.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Kuttiyadi</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-MALAPPURAM-(2).jpg" data-sub-html="<h4>Residence</h4><p>Malappuram</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-MALAPPURAM-(2).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Malappuram</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/residence/RESIDENCE-AT-MALAPPURAM-(3).jpg" data-sub-html="<h4>Residence</h4><p>Malappuram</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/residence/RESIDENCE-AT-MALAPPURAM-(3).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Residence</h3>
                                        <span>Malappuram</span>
                                    </div>
                                </a>
                            </div>
                        </li>
</ul>