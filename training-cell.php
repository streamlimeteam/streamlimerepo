<?php 
	$page_id=5;
	include('includes/header.php'); 
?>
        <div class="training_part">
            <!--<div class="head_page">
                <div class="container">
                    <h2>TRAINING CELL</h2>
                </div>
            </div>-->
            <div class="container">
                
                <div class="rd_image">
                    <div class="slider-container">
                      <div class="slider">
                        <div class="slider__item">
                          <img src="images/1.jpg" alt="">
                          <span class="slider__caption">STRUCTURAL DESIGN TRAINING ON LIVE PROJECTS<!--<a href="">Далее >></a> --></span>
                        </div>
                        <div class="slider__item">
                          <img src="images/3.jpg" alt="">
                          <span class="slider__caption">INTERNSHIP FOR CIVIL ENGINEERS (M.TECH/B.TECH/DIPLOMA)</span>
                        </div>
                        <div class="slider__item">
                          <img src="images/2.jpg" alt="">
                          <span class="slider__caption">B.TECH / M.TECH PROJECT ASSISTANCE</span>
                        </div>
                        <div class="slider__item">
                          <img src="images/4.jpg" alt="">
                          <span class="slider__caption">CIVIL ENGINEERING SOFTWARE TRAINING</span>
                        </div>
                        <div class="slider__item">
                          <img src="images/5.jpg" alt="">
                          <span class="slider__caption">INTERVIEW PREPARATION</span>
                        </div>
                        <div class="slider__item">
                          <img src="images/6.jpg" alt="">
                          <span class="slider__caption">GROUP DISCUSSION TRAINING</span>
                        </div>
                        <div class="slider__item">
                          <img src="images/7.jpg" alt="">
                          <span class="slider__caption">PERSONALITY DEVELOPMENT COURSES</span>
                        </div>
                      </div>
                        <div class="slider__switch slider__switch--prev" data-ikslider-dir="prev">
                          <span><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 20 20"><path d="M13.89 17.418c.27.272.27.71 0 .98s-.7.27-.968 0l-7.83-7.91c-.268-.27-.268-.706 0-.978l7.83-7.908c.268-.27.7-.27.97 0s.267.71 0 .98L6.75 10l7.14 7.418z"/></svg></span>
                        </div>
                        <div class="slider__switch slider__switch--next" data-ikslider-dir="next">
                          <span><svg xmlns="http://www.w3.org/2000/svg"  viewBox="0 0 20 20"><path d="M13.25 10L6.11 2.58c-.27-.27-.27-.707 0-.98.267-.27.7-.27.968 0l7.83 7.91c.268.27.268.708 0 .978l-7.83 7.908c-.268.27-.7.27-.97 0s-.267-.707 0-.98L13.25 10z"/></svg></span>
                        </div>
                    </div>
                    <!--<div id="slider">
                        <figure>
                            <img src="images/Firmenzentrale_Kaffee_Partner_-_Corporate_architecture.jpg" alt>
                            <img src="images/Firmenzentrale_Kaffee_Partner_-_Corporate_architecture.jpg" alt>
                            <img src="images/Firmenzentrale_Kaffee_Partner_-_Corporate_architecture.jpg" alt>
                            <img src="images/Firmenzentrale_Kaffee_Partner_-_Corporate_architecture.jpg" alt>
                            <img src="images/Firmenzentrale_Kaffee_Partner_-_Corporate_architecture.jpg" alt>
                        </figure>
                    </div>-->
                </div>
                <div class="boder">
                    <div class="over_view">
                     <!--   <h2>TRAINING CELL</h2> -->
						<ul style="" class="mobile_view">
							<li> STRUCTURAL DESIGN TRAINING ON LIVE PROJECTS </li>
							<li > INTERNSHIP FOR CIVIL ENGINEERS (M.TECH/B.TECH/DIPLOMA) </li>
							<li > B.TECH / M.TECH PROJECT ASSISTANCE </li>
							<li > CIVIL ENGINEERING SOFTWARE TRAINING </li>
							<li > INTERVIEW PREPARATION </li>
							<li > GROUP DISCUSSION TRAINING </li>
							<li > PERSONALITY DEVELOPMENT COURSES </li>
						</ul>
						<div class="col-lg-8 col-md-8 col-sm-8 col-lg-offset-2">
							<p>Internship Programs will include site experience plus basic of structural design.
							<br> Structural design program will include training based on structural design software. We will encourage people to have enough site visit, before entering in to the calculation part. </p>
						</div>
                    </div>
                </div>
            </div>
                <div class="training_catgories">
                    <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="rd_box">
                            <div class="rd_bx_image">
                                <img src="images/SecondaryHero_architectural_technology_bs.jpg" alt="Streamline">
                            </div>
                            <div class="rd_bx_cont">
                                <h3>INTERNSHIP PROGRAM</h3>
                                <p>We are providing internship to civil engineering diploma holders/ graduates/post graduates. The internship is including site experience and structural design. The duration of internship can be of 2 weeks or 1 months, depending on the requirements. An internship certificate will be issued on successful submission of the internship report. </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="rd_box">
                            <div class="rd_bx_image">
                                <img src="images/architects.jpg" alt="Streamline">
                            </div>
                            <div class="rd_bx_cont">
                                <h3>TRAINING PROGRAM</h3>
                                <p>The training program will be oriented towards structural design and it is aimed for graduates or post graduates in civil engineering who are interested in structural engineering. Training will be based on some live projects and structural design of an entire building will be done under this course. An experience certificate will be issued on successful submission of the training report.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-4">
                        <div class="rd_box">
                            <div class="rd_bx_image">
                                <img src="images/BCaaS.jpg" alt="Streamline">
                            </div>
                            <div class="rd_bx_cont">
                                <h3>B.TECH / M.TECH PROJECT ASSISTANCE</h3>
                                <p>We will be providing assistance to graduates or postgraduates students who are interested in structural engineering based projects. They will be given an exposure to the practical world by assigning projects based on live projects. </p>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
            </div>
            <div class="form_part train_bg">
                <div class="container">
            <div class="upload_resume training_form">
                <h1>Get Started Today!</h1>
                <p>Fill out the form below to receive more information about our in-person, online or on-site training courses. One of our representatives will contact you to answers any questions you may have.</p>
                <form method="post" action="training-enquery.php">
                    <input type="text" name="name" placeholder="Name*" required>
                    <input type="email" name="email" placeholder="Email*" required>
                    <input type="text" name="phone" placeholder="Phone">
                    <textarea name="message" placeholder="Requirement*" required></textarea>
                    <button type="submit">Submit</button>
                </form>
            </div>
            </div>
        </div>
    </div>
    <?php include('includes/footer.php');?>
    <script>
        $(".slider-container").ikSlider({
		speed: 500,
        autoPlay: true,
	});
    </script>
    </body>
</html>
