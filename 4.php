
                   <ul id="slide-gallery3" class="list-unstyled">
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Masjid-at-Kasargode.jpg" data-sub-html="<h4>Masjid</h4><p>Kasargode</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Masjid-at-Kasargode.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Masjid</h3>
                                        <span>Kasargode</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Medical-college-block--at-Truvandrum.jpg" data-sub-html="<h4>Medical college block</h4><p>Truvandrum</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Medical-college-block--at-Truvandrum.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Medical college block</h3>
                                        <span>Truvandrum</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/MOSQUE--at-TIRUR.jpg" data-sub-html="<h4>MOSQUE</h4><p>Tirur</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/MOSQUE--at-TIRUR.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>MOSQUE</h3>
                                        <span>Tirur</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Osco-Apartment-at-Calicut-(2).jpg" data-sub-html="<h4>Osco Apartment</h4><p>Calicut</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Osco-Apartment-at-Calicut-(2).jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Osco Apartment</h3>
                                        <span>Calicut</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Osco-Apartment-at-Calicut.jpg" data-sub-html="<h4>Osco Apartment</h4><p>Calicut</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Osco-Apartment-at-Calicut.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Osco Apartment</h3>
                                        <span>Calicut</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Palakad-District-Cooperative-Hospital-5.jpg" data-sub-html="<h4>District Cooperative Hospital</h4><p>Palakad</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Palakad-District-Cooperative-Hospital-5.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>District Cooperative Hospital</h3>
                                        <span>Palakad</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/PECOC-Indoor-stadium-at-perinthalmanna.jpg" data-sub-html="<h4>PECOC Indoor stadium</h4><p>Perinthalmanna</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/PECOC-Indoor-stadium-at-perinthalmanna.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>PECOC Indoor stadium</h3>
                                        <span>Perinthalmanna</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Proposed-Auditorium-at-Kondotty-(2)-5.jpg" data-sub-html="<h4>Proposed Auditorium</h4><p>Kondotty</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Proposed-Auditorium-at-Kondotty-(2)-5.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Proposed Auditorium</h3>
                                        <span>Kondotty</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Proposed-Auditorium-at-wandoor.jpg" data-sub-html="<h4>Proposed Auditorium</h4><p>Wandoor</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Proposed-Auditorium-at-wandoor.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Proposed Auditorium</h3>
                                        <span>Wandoor</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Proposed-Auditorium-Kondotty-5.jpg" data-sub-html="<h4>Proposed Auditorium</h4><p>Kondotty</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Proposed-Auditorium-Kondotty-5.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Proposed Auditorium</h3>
                                        <span>Kondotty</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Proposed-Hospital-at-tirur.jpg" data-sub-html="<h4>Proposed Hospital</h4><p>Tirur</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Proposed-Hospital-at-tirur.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Proposed Hospital</h3>
                                        <span>Tirur</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Proposed-School-building-at-KOTTUKARA-5.jpg" data-sub-html="<h4>School building</h4><p>Kottukara</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Proposed-School-building-at-KOTTUKARA-5.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>School building</h3>
                                        <span>Kottukara</span>
                                    </div>
                                </a>
                            </div>
                        </li>
</ul>