
                   <ul id="slide-gallery1" class="list-unstyled">
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/CHURCH-at-KOODATHAYI.jpg" data-sub-html="<h4>Church</h4><p>Koodathayi</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/CHURCH-at-KOODATHAYI.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Church</h3>
                                        <span>Koodathayi</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/COLLEGE-AT-WANDOOR.jpg" data-sub-html="<h4>College</h4><p>Wandoor</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/COLLEGE-AT-WANDOOR.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>College</h3>
                                        <span>Wandoor</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/College-building-at-Kollam.jpg" data-sub-html="<h4>College Bulding</h4><p>Kollam</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/College-building-at-Kollam.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>College Bulding</h3>
                                        <span>Kollam</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/College-building-at-Ramanattukara.jpg" data-sub-html="<h4>College Bulding</h4><p>Ramanattukara</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/College-building-at-Ramanattukara.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>College Bulding</h3>
                                        <span>Ramanattukara</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/College-building-for-Chinmaya-Accademy.jpg" data-sub-html="<h4>College Bulding</h4><p>Chinmaya Accademy</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/College-building-for-Chinmaya-Accademy.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>College Bulding</h3>
                                        <span>Chinmaya Accademy</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Commercial--AT-MELATUR.jpg" data-sub-html="<h4>Commercial</h4><p>Melatur</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Commercial--AT-MELATUR.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Commercial</h3>
                                        <span>Melatur</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Commercial--AT-TANALUR.jpg" data-sub-html="<h4>Commercial</h4><p>Tanalur</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Commercial--AT-TANALUR.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Commercial</h3>
                                        <span>Tanalur</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Commercial--AT-TIRUR.jpg" data-sub-html="<h4>Commercial</h4><p>Tirur</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Commercial--AT-TIRUR.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Commercial</h3>
                                        <span>Tirur</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Commercial-at-CALICUT.jpg" data-sub-html="<h4>Commercial</h4><p>Calicut</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Commercial-at-CALICUT.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Commercial</h3>
                                        <span>Calicut</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Commercial-at-Koodaranji.jpg" data-sub-html="<h4>Commercial</h4><p>Koodaranji</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Commercial-at-Koodaranji.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Commercial</h3>
                                        <span>Koodaranji</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Commercial-at-Malappuram.jpg" data-sub-html="<h4>Commercial</h4><p>Malappuram</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Commercial-at-Malappuram.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Commercial</h3>
                                        <span>Malappuram</span>
                                    </div>
                                </a>
                            </div>
                        </li>
                        <li class="col-xs-6 col-lg-4 col-md-4 col-sm-4 mobile_gallery" data-src="images/portfolio/Commercial-at-MANJERI.jpg" data-sub-html="<h4>Commercial</h4><p>Malappuram</p>">
                            <div class="gallery_box">
                                <a href="javascript:void(0)" class="hover"> <img class="img-responsive" src="images/portfolio/Commercial-at-MANJERI.jpg"> 
                                    <div class="overlay over_show">
                                        <h3>Commercial</h3>
                                        <span>Manjeri</span>
                                    </div>
                                </a>
                            </div>
                        </li>
</ul>